��    ^           �      �     �                    1     5     E     I     P     c     i     �     �     �     �     �     �     �     �     	     	     4	     <	     N	     Z	     f	     n	     �	  
   �	     �	     �	     �	     �	     �	  
   �	     �	     �	     
     
     
     2
     ?
     E
  	   V
     `
  
   i
     t
     y
     �
     �
     �
     �
     �
     �
     �
  
   �
     �
     �
                    "     *     9     I  
   V     a     r     w     }     �     �     �     �     �     �     �     �     �     �     �               *     7     E     L  
   U  	   `  	   j     t     y     �  �  �               #     ?     Y     _     q  	   y     �     �     �     �  &   �     �  "        6     >     G     W     r     �     �     �     �     �     �     �     �  
   �  
   
  
              6     I     V     d     w     �     �     �     �     �     �     �     �     �  
             ,     =     L     T     b     v          �     �     �     �  	   �  
   �     �     �     �          "     /     A  	   G     Q     g     x  	   �     �     �     �     �     �     �     �     �     �          '     8     N  
   W     b     n     z     �     �     �     2                               <       %       W   X      '   I   )       7       -              Y          +                  !   R   V   G       "      0       4         :       L                6       3          ]            U   A   ;   \   N         S       .   ,   /       K   P   1   ^   O   [      H   E   $   T       	   Q          ?           B   9                     F   J   C   8   >       &       @   Z          
   #                 *      D   (   5   M   =    (empty) ... 504: Server error <%=value%> (<%=percent%>) Age Age: <%=value%> All Answer Answer: <%=value%> Audit Average Complete Views Average Correct Average Incomplete Views Average Incorrect Average Submissions Per Problem Clear Cohort Cohort Groups Cohort: <%= filterVal %> Complete Views Completion Percentage Correct Country or Region Course List Course Name Current Current Enrollment Date Difficulty Discussions Download CSV Education: <%=value%> Educational Background End Date Enrollment Enrollment Mode Female Find a course Find a learner Gender: <%=value%> Generic List Honor Incomplete Views Incorrect Learners Loading... Male Name (Username) Not Reported Number of Learners Order Other Page Not Found Percent Percent of Total Percentage Percentage Correct Problems Professional Programs Replays Results Search courses Search learners Server error Start Date Submission Count Time Total Total Enrollment Tried a Problem Unique Viewers Unknown Upcoming Value Variant Verified Videos Videos Viewed Watched a Video Week Ending Week Ending <%=value%> availability clear search click to sort cohort disabled first page last page next page page previous page search Project-Id-Version: edx-platform
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-11-14 11:32+0000
Last-Translator: Abel Camacho <abelcama@gmail.com>
Language-Team: Basque (Spain) (http://www.transifex.com/open-edx/edx-platform/language/eu_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: eu_ES
Plural-Forms: nplurals=2; plural=(n != 1);
 (hutsik) ... 504: Zerbitzariaren errorea <%=value%> (<%=percent%>) Adina Adina: <%=value%> Guztiak Erantzuna Erantzuna: <%=value%> Entzulea Batez besteko ikustaldi osatuak Batez besteko zuzena Batez besteko osatu gabeko ikustaldiak Batez besteko okerra Batez besteko bidalketak ariketako Garbitu Kohortea Kohorte-taldeak Kohortea: <%= filterVal %> Ikustaldi osatuak Osaketa-portzentajea Zuzena Herria edo herrialdea Ikastaro-zerrenda Ikastaroaren izena Oraingoa Oraingo matrikulazioa Data Zailtasuna Eztabaidak Jaitsi CSV Hezkuntza: <%=value%> Egindako ikasketak Amaiera-data Matrikulazioa Matrikulazio-modua Emakumezkoa Bilatu ikastaroa Bilatu ikaslea Sexua: <%=value%> Zerrenda orokorra Portaera Osatu gabeko ikustaldiak Okerra Ikasleak Kargatzen... Gizonezkoa Izena (erabiltzaile-izena) Jakinarazi gabea Ikasle-kopurua Agindua Bestelako bat Ez da orria aurkitu Ehunekoa Denerakoaren ehunekoa Portzentajea Portzentaje zuzena Ariketak Profesionala Programak Erantzunak Emaitzak Bilatu ikastaroak Bilatu ikasleak Zerbitzariaren errorea Hasiera-data Bidalketa-kopurua Ordua Denerakoa Matrikulazioak denera Egindako ariketa Ikuskatzaile bakarrak Ezezaguna Laster Balorea Aldagaia Egiaztatuta Bideoak Bideoak ikusi dira Ikusitako bideoa Astearen amaiera Astearen amaiera <%=value%> Eskuragarritasuna garbitu bilaketa klik egin ordenatzeko Kohortea desgaituta lehen orria azken orria hurrengo orria orria aurreko orria bilatu 