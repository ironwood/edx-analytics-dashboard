��    z      �  �   �      H
     I
     h
     ~
     �
     �
     �
     �
     �
     �
     �
     �
     �
       	     
        &     ?     E     M     j  	   v     �  O   �     �  
   �     �            	   (     2  �   ?  �   �  	   �     �  
   �     �     �  
   �               +     7     F     M     \     c  	   {     �     �  #   �     �     �     �     �          "     8     J  (   _  *   �     �     �     �     �     �     �  
   �  
   �     �     �                 &   0     W     c     r     z     �     �  	   �     �     �     �     �     �               '     4  
   D     O     ]     d     v  "   �  6   �     �     �          #     4     E  	   W     a     i     y     �  
   �     �     �  	   �     �  !   �  !   �  E   !  ,   g     �     �     �     �  �  �      h     �     �     �     �     �     �     �     �            
   '     2     D  	   I  #   S     w     |     �     �     �     �  d   �  
   =  	   H     R  #   h  
   �  
   �  
   �  �   �  �   j  	   +     5     B     J     \     n     |  $   �     �     �     �     �     �     �  	             2     ;     Z     z     �  &   �     �     �     �     	  "   $  +   G     s     {     �  
   �     �     �     �     �  
   �     �     �     �       '        C     Q     `     p     �     �     �     �     �     �     �     	          )     <     L     _     l     |     �  
   �  )   �  6   �          !     4     L     `     v     �  	   �     �     �     �     �                     )     I     d  P   |  (   �     �           +      >      ?   >                   K          *       t   `       L   R   -   k   X   g         J       z              C   2   =   "   @   3       4       O                 )   (   Q       \   '      
              D   i   b          p               M       P       o   c       j   ;      h   I   f           A   y   S   <      ,   a                   $          /   %   w   W   x           _       u   n         Y       v   5   V      [   G         #   &   q   .   E   H   ^   9   +       d   	   6   ]   F                             e   B   7   1           Z   s   m   T   :   8          0   r      !   N      l   U              
        Select %(level)s
     %(all_sections_text)s %(policy_ratio)s%% Access Denied Activity Age All Assignments All Learners All Outcomes All Sections An Error Occurred Application Assignment Name Associate Bachelor's Basic Course Information Close Content Content Engagement Breakdown Course Home Course ID Course Name Course engagement data was last updated %(update_date)s at %(update_time)s UTC. Courses Courseware Current Enrollment Daily Learner Enrollment Demographics Doctorate Download CSV Each bar shows the average number of complete and incomplete views for videos in that section. Click on bars with low totals or a high incomplete rate to drill down and understand why. Each bar shows the average number of complete and incomplete views for videos in that subsection. Click on bars with low totals or a high incomplete rate to drill down and understand why. Education End Date Engagement Engagement Content Engagement Videos Enrollment Enrollment Metrics Enrollment by Country Enrollments External Tools Female Finished Video Gender Geographic Distribution Geography Geography Metrics Help How many learners are in my course? How old are my learners? In Progress Instructor Dashboard Join the Open Source Community Learners Learners 25 and Under Learners 26 to 40 Learners 41 and Over Learners who started watching the video. Learners who watched the video to the end. Loading Login Logout Male Master's Middle Need Help? Next Video None Not Started Yet Other Page Not Found Part {part_number} Part {part_number}: {part_description} Performance Previous Video Primary Privacy Policy Problem Name Research at edX Secondary Section Name Section Submissions Select Assignment Type Select Outcome Select Problem Select Section Select Subsection Select Video Skip to content Start Date Started Video Status Submission Counts Submissions Submissions for Part {part_number} Submissions for Part {part_number}: {part_description} Submissions: {part_description} Subsection Name Subsection Submissions Terms of Service Total Enrollment Total Video Views Try Again Unknown Unknown Country Verified Enrollment Video Metrics Video Name Video Views Videos View Live Watched a Video Last Week We are unable to load this chart. We are unable to load this table. We're having trouble loading this page. Please try again in a minute. What level of education do my learners have? Where are my learners? Who are my learners? {part_description} {statistic}% Project-Id-Version: edx-platform
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-11-15 12:43+0000
Last-Translator: Abel Camacho <abelcama@gmail.com>
Language-Team: Basque (Spain) (http://www.transifex.com/open-edx/edx-platform/language/eu_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: eu_ES
Plural-Forms: nplurals=2; plural=(n != 1);
 
        Aukeratu %(level)s
     %(all_sections_text)s %(policy_ratio)s%% Sarbidea ukatu da Jarduera Adina Zeregin guztiak Ikasle guztiak Ikas-emaitza guztiak Atal guztiak Errorea gertatu da Aplikazioa Zereginaren izena Lotu Lizentzia Ikastaroaren oinarrizko informazioa Itxi Edukia Jardunaren sailkapena edukietan Ikastaro-hasiera Ikastaroaren IDa Ikastaroaren izena Ikastaroko jardunaren azken eguneraketa %(update_date)s egunean izan zen %(update_time)s UTC orduan. Ikastaroak Ikasgunea Oraingo matrikulazioa Egunez eguneko ikasle-matrikulazioa Demografia Doktoregoa Jaitsi CSV Barra bakoitzak atal honetako bideoak osorik eta zatika zenbat aldiz ikusi diren erakusten du. Sakatu denerako txikia edo zatikako altua duten barrei zer gertatu den eta zergatik jakiteko. Barra bakoitzak azpiatal honetako bideoak osorik eta zatika zenbat aldiz ikusi diren erakusten du. Sakatu denerako txikia edo zatikako altua duten barrei zer gertatu den eta zergatik jakiteko. Hezkuntza Amaiera-data Jarduna Jarduna edukietan Jarduna bideoetan Matrikulazioa Matrikulazio-metrikak Matrikulazioak herrialdearen arabera Matrikulazioak Kanpoko tresnak Emakumezkoa Amaitutako bideoa Sexua Banaketa geografikoa Geografia Estatistika geografikoak Laguntza Zenbat ikasle dago ikastaroan? Zenbat urte dute nire ikasleek? Lanean Irakaslearen aginte-panela Bat egin Kode irekiko komunitatearekin Ikasleak 25 urtetik beherako ikasleak 26 - 40 urte bitarteko ikasleak 41 urtetik gorako ikasleak Bideoa ikusten hasi diren ikasleak Amaierara arte bideoa ikusi duten ikasleak. Igotzen Sartu Irten Gizonezkoa Masterra Erdia Laguntza behar duzu? Hurrengo bideoa Bat ere ez Oraindik hasi gabea Bestelako bat Ez da orria aurkitu Atala {part_number} {part_number} atala: {part_description} Performantzia Aurreko bideoa Lehen hezkuntza Pribatutasun-politika Ariketaren izena Ikerketa edX-en Bigarren Hezkuntza Atalaren izena Atalaren bidalketak Aukeratu zeregin-mota Aukeratu ikas-emaitza Aukeratu ariketa Aukeratu atala Aukeratu azpiatala Aukeratu bideoa Egin jauzi edukira Hasiera-data Hasitako bideoa Egoera Bidalketa-kopuruak Bidalketak Bidalketak atal honetarako: {part_number} {part_number} atalerako bidalkeatk: {part_description} Bidalketak: {part_description} Azpiatalaren izena Azpiatalaren bidalketak Zerbitzu-baldintzak Matrikulazioak denera Denerako bideo-ikustaldiak Saiatu berriz Ezezaguna Herrialde ezezaguna Egiaztatutako matrikulazioa Bideoaren estatistikak Bideoaren izena Bideo-ikustaldiak Bideoak Ikusi zuzenean Aurreko astean ikusitako bideoa Ezin dugu diagrama hau igo Ezin dugu taula hau igo Hainbat arazo ditugu orri hau igotzeko. Mesedez, saiatu berriz minutu bat barru. Zein hezkuntza-maila dute nire ikasleek? Non daude nire ikasleak? Nortzuk dira nire ikasleak? {part_description} {statistic}% 